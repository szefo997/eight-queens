export class AppInit {
  public static readonly type = '[App] Init';

  constructor() {
  }
}

export class AppReset {
  public static readonly type = '[App] Reset';

  constructor() {
  }
}

export class AppPrevious {
  public static readonly type = '[App] Previous action';

  constructor() {
  }
}

export class AppNext {
  public static readonly type = '[App] Next action';

  constructor() {
  }
}

