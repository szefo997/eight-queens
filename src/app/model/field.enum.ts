export enum Field {

  /**
   * field is in use
   */
  OCCUPIED = 1,
  /**
   * field is not in use
   */
  FREE = 0

}
