import {Field} from './field.enum';

export default class Board {

  public columns: number[];
  public diagDown: number[];
  public diagUp: number[];

  public readonly solutions: number[][] = [];

  constructor(private numColumns: number) {
    this._init();
  }

  /**
   * Starts the search with initial parameters
   */
  public run(): void {
    this._tryNewQueen(0);
  }

  /**
   * Searches for all possible solutions
   * @param row - row of the board
   */
  private _tryNewQueen(row: number): void {
    for (let column = 0; column < this.numColumns; column++) {

      // current column blocked
      if (this.columns[column] >= 0) {
        continue;
      }

      // relating diagonale '\' depending on current row and column
      const diagDownIndex = row + column;
      if (this.diagDown[diagDownIndex] === Field.OCCUPIED) {
        continue;
      }

      // relating diagonale '/' depending on current row and column
      const diagonalUpIndex = this.numColumns - 1 - row + column;
      if (this.diagUp[diagonalUpIndex] === Field.OCCUPIED) {
        continue;
      }

      // occupying column and diagonals depending on current row and column
      this.columns[column] = row;
      this.diagDown[diagDownIndex] = Field.OCCUPIED;
      this.diagUp[diagonalUpIndex] = Field.OCCUPIED;

      if (row === this.numColumns - 1) {
        this.solutions.push(this.columns.slice(0));
      } else {
        this._tryNewQueen(row + 1);
      }

      // reset for new solution
      this.columns[column] = -1;
      this.diagDown[diagDownIndex] = Field.FREE;
      this.diagUp[diagonalUpIndex] = Field.FREE;
    }
  }

  private _init(): void {
    this.columns = new Array(this.numColumns);
    // 15 for 8 columns
    const numberOfDiagonals = 2 * this.numColumns - 1;
    this.diagDown = new Array(numberOfDiagonals);
    this.diagUp = new Array(numberOfDiagonals);

    // clear all
    for (let index = 0; index < numberOfDiagonals; ++index) {
      if (index < this.numColumns) {
        this.columns[index] = -1;
      }
      this.diagDown[index] = Field.FREE;
      this.diagUp[index] = Field.FREE;
    }
  }

}
