import {Action, Selector, State, StateContext} from '@ngxs/store';
import {AppInit, AppNext, AppPrevious, AppReset} from './app.actions';
import Board from './model/board';

export interface AppStateModel {
  numColumns: number;
  allSolutions: number[][];
  solutionsQyt: number;
  currentSolution: number;
  columnNames: string[];
}

export const APP_DEFAULTS = {
  allSolutions: [],
  solutionsQyt: null,
  currentSolution: 1,
  numColumns: 8,
  columnNames: [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H'
  ],
};

@State<AppStateModel>({
  name: 'app',
  defaults: APP_DEFAULTS
})
export class AppState {

  @Selector()
  public static appState(state: AppStateModel): AppStateModel {
    return state;
  }

  @Selector()
  public static solutionsQyt({solutionsQyt}: AppStateModel): number {
    return solutionsQyt;
  }

  @Selector()
  public static currentSolution({currentSolution}: AppStateModel): number {
    return currentSolution;
  }

  @Selector()
  public static numColumns({numColumns}: AppStateModel): number {
    return numColumns;
  }

  @Selector()
  public static allSolutions({allSolutions}: AppStateModel): number[][] {
    return allSolutions;
  }

  @Action(AppInit)
  public init({patchState, getState}: StateContext<AppStateModel>) {
    const {numColumns} = getState();
    const board = new Board(numColumns);
    const start = new Date().getTime();
    board.run();
    console.log(`...calculation took ${(new Date().getTime() - start)} ms`);
    const {solutions} = board;
    patchState({currentSolution: 1, solutionsQyt: solutions.length, allSolutions: solutions});
  }

  @Action(AppReset)
  public resetApp({setState}: StateContext<AppStateModel>) {
    setState(APP_DEFAULTS);
  }

  @Action(AppNext)
  public next({getState, patchState}: StateContext<AppStateModel>) {
    const {currentSolution, allSolutions} = getState();
    if (currentSolution < allSolutions.length) {
      patchState({currentSolution: currentSolution + 1});
    }
  }

  @Action(AppPrevious)
  public previous({getState, patchState}: StateContext<AppStateModel>) {
    const {currentSolution} = getState();
    if (currentSolution > 1) {
      patchState({currentSolution: currentSolution - 1});
    }
  }

}
