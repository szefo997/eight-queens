import {Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {AppInit, AppNext, AppPrevious} from './app.actions';
import {AppState} from './app.state';
import {Observable} from 'rxjs';

@Component({
  selector: 'rb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @Select(AppState.numColumns) public numColumns$: Observable<number>;
  @Select(AppState.currentSolution) public currentSolution$: Observable<number>;
  @Select(AppState.solutionsQyt) public solutionsQyt$: Observable<number>;
  @Select(AppState.allSolutions) public allSolutions$: Observable<number[][]>;

  constructor(private _store: Store) {
  }

  ngOnInit(): void {
    this._store.dispatch(new AppInit());
  }

  public onPrevious(): void {
    this._store.dispatch(new AppPrevious());
  }

  public onNext(): void {
    this._store.dispatch(new AppNext());
  }

}
