import {Component} from '@angular/core';

@Component({
  selector: 'rb-header',
  template: `
      <header class="header mb-3">
          <div class="container">
              <div class="display-4 font-weight-light text-white">
                  <span class="h3 font-weight-light">Eight Queens</span>
              </div>
          </div>
      </header>
  `,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor() {
  }

}
