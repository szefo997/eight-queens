import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {APP_DEFAULTS, AppState} from './app.state';
import {AppInit, AppNext, AppPrevious, AppReset} from './app.actions';

describe('App state', () => {
  let store: Store;

  beforeEach(async(async () => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([AppState])]
    }).compileComponents();
    store = TestBed.get(Store);
    await store.dispatch(new AppReset()).toPromise();
  }));

  it('should reset app state', async () => {
    await store.dispatch(new AppReset()).toPromise();
    const appState = store.selectSnapshot(AppState.appState);
    expect(appState.currentSolution).toEqual(APP_DEFAULTS.currentSolution);
    expect(appState.allSolutions).toEqual(APP_DEFAULTS.allSolutions);
    expect(appState.solutionsQyt).toEqual(APP_DEFAULTS.solutionsQyt);
    expect(appState.numColumns).toEqual(APP_DEFAULTS.numColumns);
    expect(appState.columnNames).toEqual(APP_DEFAULTS.columnNames);
  });

  it('should init app', async () => {
    await store.dispatch(new AppInit()).toPromise();
    const appState = store.selectSnapshot(AppState.appState);
    expect(appState.columnNames).toEqual(APP_DEFAULTS.columnNames);
    expect(appState.numColumns).toEqual(APP_DEFAULTS.numColumns);
    expect(appState.currentSolution).toEqual(1);
    expect(appState.allSolutions.length).toBeGreaterThan(0);
    expect(appState.solutionsQyt).toBeGreaterThan(1);
  });

  it('should go to the next solution', async () => {
    await store.dispatch(new AppInit()).toPromise();
    await store.dispatch(new AppNext()).toPromise();
    expect(store.selectSnapshot(AppState.currentSolution)).toEqual(2);
  });

  it('should go to the previous solution', async () => {
    await store.dispatch(new AppInit()).toPromise();
    await store.dispatch(new AppNext()).toPromise();
    expect(store.selectSnapshot(AppState.currentSolution)).toEqual(2);
    await store.dispatch(new AppPrevious()).toPromise();
    expect(store.selectSnapshot(AppState.currentSolution)).toEqual(1);
  });

});
