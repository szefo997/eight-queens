import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {NgxsModule} from '@ngxs/store';
import {DEVTOOLS_REDUX_CONFIG, LOGGER_CONFIG, OPTIONS_CONFIG, STATES_MODULES} from './store.config';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';
import {ForNumberPipe} from './for-number.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ForNumberPipe
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    NgxsModule.forRoot(STATES_MODULES, OPTIONS_CONFIG),
    NgxsReduxDevtoolsPluginModule.forRoot(DEVTOOLS_REDUX_CONFIG),
    NgxsLoggerPluginModule.forRoot(LOGGER_CONFIG)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
